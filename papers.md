---
title: Papers
layout: default
permalink: /papers/
---

# Google Scholar
[Recent First](!!!), [Most Cited First](!!!)

# Conference
{% bibliography --file conference %}

# Journal
{% bibliography --file journal %}

# Technical Reports
{% bibliography --file techreportandarxiv %}

# Book Chapters
{% bibliography --file chapters %}

# Invited Papers
{% bibliography --file invited %}

# Workshop Publications
{% bibliography --file workshop %}
